import { takeEvery, all } from "redux-saga/effects";
import userSaga from "./userSaga";

export default function* watch() {
  yield all([takeEvery("FETCH_USER_REQUEST", userSaga)]);
}
