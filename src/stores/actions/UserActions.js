// import {getUser} from '../../api/FakeApiUser';

export const fetchUserRequest = (username, password) => {
  return {
    type: "FETCH_USER_REQUEST",
    username,
    password,
  };
};

export const fetchUserSuccess = (users) => {
  return {
    type: "FETCH_USER_SUCCESS",
    payload: users,
  };
};

export const fetchUserFail = () => {
  return {
    type: "FETCH_USER_FAILED",
  };
};

// export const fetchDataUser = () => async dispatch => {
//   try {
//     dispatch(fetchUserRequest());
//     const {data} = await getUser();
//     dispatch(fetchUserSuccess(data));
//   } catch (error) {
//     dispatch(fetchUserFail());
//   }
// };
