import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  safeAreaView1: { backgroundColor: "#FFF", flex: 0 },
  safeAreaView2: { flex: 1, backgroundColor: "#FFF" },
  outerWrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#red",
  },
  buttonStyle: {
    backgroundColor: "#EEE",
    paddingHorizontal: 40,
    paddingVertical: 30,
    borderWidth: 0.5,
    borderColor: "#F0F0F0",
    borderRadius: 10,
  },
  text: { fontSize: 18, color: "#808080", fontWeight: "bold" },
  userContainer: {
    borderBottomWidth: 1,
    borderColor: "#eee",
    padding: 1,
    marginTop: 10,
  },
  container: {
    flex: 1,
  },
  item: {
    // padding: 20,
    height: 100,
    margin: 8,
    // marginHorizontal: 16,
  },
  title: {
    fontSize: 22,
    color: "black",
  },
  subTitle: {
    fontSize: 20,
    color: "grey",
  },
});

export default Styles;
