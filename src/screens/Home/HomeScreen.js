import React, { useEffect } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
} from "react-native";
import styles from "./Styles";

const HomeScreen = ({ navigation }) => {
  const [restaurantList, setRestaurantList] = React.useState([]);
  const [filteredList, setFilteredList] = React.useState([]);
  const [images, setImages] = React.useState([]);
  const [input, setInput] = React.useState("");

  useEffect(() => {
    fetch(
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/TopRamen8d30951.json"
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setRestaurantList(data);
        setFilteredList(data);
        // Work with JSON data here
        console.log(data);
      })
      .catch((err) => {
        // Do something for an error here
      });
    fetch(
      "https://s3-ap-southeast-1.amazonaws.com/he-public-data/noodlesec253ad.json"
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setImages(data);
        // Work with JSON data here
        console.log(data);
      })
      .catch((err) => {
        // Do something for an error here
      });
  }, []);

  const search = (word) => {
    const filtered = filteredList.filter((data) => {
      return data.Brand.includes(word);
    });
    setRestaurantList(filtered);
  };
  const Item = ({ item }) => {
    let value = Math.floor(Math.random() * 5 + 1);

    return (
      <TouchableOpacity
        style={styles.item}
        onPress={() => {
          navigation.navigate("Profile", {
            item: item,
            image: images[value].Image,
          });
        }}
      >
        <View
          style={{
            alignItems: "center",
            flexDirection: "row",
          }}
        >
          <Image
            style={{ width: 150, height: 100 }}
            source={{ uri: images[value].Image }}
          />
          <View style={{ marginLeft: 10 }}>
            <Text style={styles.title}>{item.Brand}</Text>
            <Text style={styles.subTitle}>{`country:${item.Country}`}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const renderItem = ({ item, index }) => (
    <Item title={item.Brand} index={index} item={item} />
  );
  return (
    <>
      <SafeAreaView style={styles.safeAreaView2}>
        <TextInput
          style={{
            marginTop: 20,
            height: 40,
            width: "70%",
            borderColor: "green",
            borderWidth: 1,
            marginLeft: 30,
            paddingHorizontal: 10,
          }}
          onChangeText={(text) => {
            setInput(text);
            search(text);
          }}
          value={input}
          placeholder="Search...."
          placeholderTextColor={"grey"}
        />
        {images.length > 0 && (
          <FlatList
            style={{ flex: 1, marginTop: 30 }}
            data={search !== "" ? restaurantList : filteredList}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
          />
        )}
      </SafeAreaView>
    </>
  );
};

export default HomeScreen;
