import React from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
} from "react-native";

const Profile = ({ route, navigation }) => {
  const details = route.params.item;
  const image = route.params.image;

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        style={{ height: 100, width: 150 }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Text style={styles.title}>{"Back"}</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.item} onPress={() => {}}>
        <View
          style={{
            alignItems: "center",
            flexDirection: "row",
          }}
        >
          <Image style={{ width: 150, height: 100 }} source={{ uri: image }} />
          <View style={{ marginLeft: 10 }}>
            <Text style={styles.title}>{details.Brand}</Text>
            <Text style={styles.subTitle}>{`country:${details.Country}`}</Text>
            <Text style={styles.subTitle}>{`Stars:${details.Stars}`}</Text>
            <Text style={styles.subTitle}>{`Style:${details.Style}`}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
export default Profile;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    marginLeft: 20,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 20,
  },
});
