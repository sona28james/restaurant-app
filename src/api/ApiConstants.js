/*
 * App config for apis
 */

// import { LOGIN_CREW_GROUP } from './constants';
const ApiConstants = {
  // BASE_URL: 'http://dev.dlm.com/',
  BASE_URL: "http://103.127.147.163/",
  SIGN_IN: "authenticate/",
  SIGN_OUT: "logout-user/",

  // Authentication
  LOGIN: "login/",
  LOGOUT: "logout/",
  REGISTER: "crew/crew-registration/",
  FORGOT_PASSWORD: "crew/forgot-password/",
  CHANGE_PASSWORD: "crew/changepassword/",

  // Common
  GET_NATIONALITIES: "crew/country",
  GET_TIMEZONES: "crew/timezone",
  SAVE_FCM_TOKEN: "user/save-fcm-token/",

  // Affiliates
  GET_AFFILIATES: "crew/affiliate-list/",
  GET_AFFILIATE_DETAILS:
    "crew/affiliate-detailed-list/{affiliateId}?status={status}",

  // Shifts
  GET_SHIFT_LIST:
    "crew/crew-shift-list/?page={page}&page_size={page_size}&status={status}&search={search}&column={column}",
  GET_SHIFT_STATUS_COUNT: "crew/crew-shift-status-count/?search={search}",
  UPDATE_SHIFT: "crew/change-scheduled-shift-status",
  UPDATE_SHIFT_CHEK_IN_OUT: "crew/check-in-out-status-change",
  GET_SHIFT_SUMMARY: "crew/shift-summary/",
  GET_SHIFT_DETAILS: "crew/crew-shift-details/{shiftId}",

  // Shift Filter
  SHIFT_FILTER_AFFILIATE: "crew/shift-filter-affiliate-list/",
  SHIFT_FILTER_CLIENT: "crew/shift-filter-client-list/",
  SHIFT_FILTER_EVENT: "crew/shift-filter-event-list/",
  SHIFT_FILTER_SKILL: "crew/shift-filter-crew-skill-list/",

  // TimeSheet filter
  TIME_SHEET_FILTER_AFFILIATE: "crew/crew-affiliate-filter-list/",
  TIME_SHEET_FILTER_CLIENT: "crew/crew-client-filter-list/",
  TIME_SHEET_FILTER_EVENT: "crew/crew-event-filter-list/",
  TIME_SHEET_FILTER_SKILL: "crew/crew-skill-filter-list/",
  TIME_SHEET_FILTER_SHIFT: "crew/crew-shift-filter-list/",

  // Unavailability
  GET_UNAVAILABILITY: "crew/crew-unavailability/?year={year}&month={month}",
  DELETE_UNAVAILABILITY: "crew/crew-unavailability/{id}",
  CREATE_UNAVAILABILITY: "crew/crew-unavailability/",

  // Timesheet
  GET_TIMESHEET_LIST:
    "crew/crew-timesheet-details/?page={page}&page_size={page_size}&column={column}",
  GET_CREW_MEMBERS_LIST: "crew/shift-crew-member-list/?page={page}",
  GET_CREW_MEMBER_TIMESHEET_LIST: "crew/shift-crew-member-details/",
  EDIT_TIMESHEET: "crew/edit-crew-timesheet/{timesheet_id}",
  GET_AFFILIATE_OTHER_EXPENSES_LIST:
    "crew/other-expenses-master/?affiliate={affiliateId}",
  ADD_UPDATE_OTHER_EXPENSES: "crew/add-update-timesheet-expense/",
  DELETE_OVERTIME_REQUEST: "crew/delete-ot-request/{timesheet_id}",
  DELETE_OTHER_EXPENSE_REQUEST: "crew/delete-oe-request/{other_expenses_id}",

  // Feedback
  PROVIDE_FEEDBACK: "crew/crew-feedback/{feedback_id}",
  GET_FEEDBACK_LIST: "crew/crew-feedback/",
  GET_SHIFT_FEEDBACK_BY_CHIEF: "crew/chief-shift-review/?shift_id={shift_id}",
  ADD_SHIFT_FEEDBACK_BY_CHIEF: "crew/chief-shift-review/",
  EDIT_SHIFT_FEEDBACK_BY_CHIEF: "crew/chief-shift-review/{shift_id}",

  // Notifications
  GET_NOTIFICATIONS_LIST:
    "crew/crew-notification-list/?page={page}&page_size={page_size}",
  MARK_NOTIFICATION_AS_READ:
    "crew/mark-notification-as-read/?user_notif_id={user_notif_id}",
  GET_NOTIFICATIONS_UNREAD_COUNT: "crew/crew-notification-unread-count/",
  MARK_ALL_NOTIFICATIONS_AS_READ: "make-all-notifications-read/",

  // User Profile
  USER_DETAILS: "crew/crew-list-update/",
  DELETE_DOCUMENT_FILE: "crew/crew-delete-file/{fileId}",
  DELETE_CREW_DOCUMENT: "crew/crew-delete-document/{documentId}",
  DELETE_USER_SKILL: "crew/crew-delete-skill/{skillId}",
  USER_DOCS_DETAILS: "crew/crew-document-master/",

  // Terms & Conditions
  // TERMS_LOAD: "user/terms-and-conditions-list/?group=" + LOGIN_CREW_GROUP,
  TERMS_UPDATE: "user/terms-and-conditions-log/",

  // CHECK APP VERSION UPDATE
  VERSION_DETAILS: "super-admin/version-list/",

  // GET CREW OFFSET INCLUDING DAYLIGHT SAVING TIME
  CREW_TIMEZONE_DATE: "super-admin/server-time/",
};

export default ApiConstants;
