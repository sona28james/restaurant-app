/*
 *General api to access data
 */

import ApiConstants from './ApiConstants';

export default function api(path, params, method, token) {
  let options;

  let headerVal = new Headers({
    Authorization: 'token ' + token,
    'Content-Type': 'application/json',
  });
  let headerValAuthenticate = new Headers({
    'Content-Type': 'application/json',
  });
  options = Object.assign(
    {headers: token ? headerVal : headerValAuthenticate},
    {method: method},
    params
      ? {
          body: JSON.stringify(params),
        }
      : null,
  );

  return fetch(ApiConstants.BASE_URL + path, options)
    .then(resp => resp.json())
    .then(json => json)
    .catch(error => error.message);
}
