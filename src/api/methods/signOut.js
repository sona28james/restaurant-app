/**
 * Api function for logout session
 * @param {*} token
 */
import Api from '@api';
import ApiConstants from '../ApiConstants';

export default function signOut(token) {
  return Api(ApiConstants.SIGN_OUT, null, 'get', token);
}
