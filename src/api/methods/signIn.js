/**
 * Api function for login Session creation and authentication
 * @param {*} _username
 *  @param {*} _password
 */
import Api from '@api';
import ApiConstants from '../ApiConstants';

export default function signIn(_username, _password) {
  return Api(
    ApiConstants.SIGN_IN,
    {
      username: _username,
      password: _password,
    },
    'post',
  );
}
