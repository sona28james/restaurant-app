/**
 * Api function for login Session creation and authentication
 * @param {*} _username
 *  @param {*} _password
 */
import Api from "@api";
import ApiConstants from "../ApiConstants";

export default function register(_username, _password) {
  console.log("registercalled");
  return Api(
    ApiConstants.REGISTER,
    {
      username: _username,
      password: _password,
    },
    "post"
  );
}
